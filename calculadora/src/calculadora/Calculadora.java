package calculadora;

import java.util.Scanner;

public class Calculadora
{   
    private static float cubo(float numero1)
    {
        float respuesta;
        respuesta = numero1*numero1*numero1;
        System.out.println("El cubo de " + numero1 + " es igual a: " + respuesta);
        return respuesta;
    }
    public static float suma(float numero1, float numero2)
    {
        float respuesta;
        respuesta = numero1+numero2;
        System.out.println("La multiplicacion de " + numero1 + " con " + numero2 + " es igual a: " + respuesta);
        return respuesta;
    }
    public static float resta(float numero1, float numero2)
    {
        float respuesta;
        respuesta = numero1-numero2;
        System.out.println("La multiplicacion de " + numero1 + " con " + numero2 + " es igual a: " + respuesta);
        return respuesta;
    }
               
     public static float multiplicar(float numero1, float numero2)
    {
        float respuesta;
        respuesta = numero1*numero2;
        System.out.println("La multiplicacion de " + numero1 + " con " + numero2 + " es igual a: " + respuesta);
        return respuesta;
    }
    public static float Dividir(float numero1, float numero2)
    {
        float respuesta;
        if (numero2!=0){
            respuesta = numero1/numero2;
            System.out.println("La multiplicacion de " + numero1 + " con " + numero2 + " es igual a: " + respuesta);
        }
        else {
            respuesta=0;
            System.out.println("no existe operacion");     
        }
        return respuesta;
    }
        
    public static void main(String[] args)
    {
        Scanner leernumero = new Scanner(System.in);
        
        int opcion;
        float numero1;
        float numero2;
        System.out.print("Escriba el numero 1:");
        numero1 = leernumero.nextFloat();
        System.out.print("Escriba el numero 2:");
        numero2 = leernumero.nextFloat();

        do
        {
            System.out.println("1. Sumar");
            System.out.println("2. Restar");       
            System.out.println("3. Multiplicar");
            System.out.println("4. Dividir");
            System.out.println("5. Cubo de Numero 1");
            System.out.println("6. Salir");
            System.out.print("Escriba una opcion:");
            opcion = leernumero.nextInt();
            switch(opcion)
            {
                case 1: suma(numero1,numero2);
                        break;
                case 2: resta(numero1,numero2);
                        break; 
                case 3: multiplicar(numero1,numero2);
                        break; 
                case 4: Dividir(numero1,numero2);
                        break;
                case 5: cubo(numero1);
                        break;                        
                case 6: System.exit(0);
                        break;
                default: System.out.println("No selecciono una opcion valida");
                        break;
            }
        }
        while(opcion != 5);
        
    } 
}

   
